import sys
import random

str = 'This is a long string that came off the top of my mind and does not make any sense because my mind is not working at the moment and probably because it is so occupied at the moment. This is so not true. This might be a fact. A dog barked at me. A string is a long list of characters. It must be processed and discarded.'

def construct(string):
    dict = {}
    words = str.split()

    current = words[0]

    for word in words[1:]:
        if not current in dict:
            dict[current] = [word]
        else:
            if not word in dict[current]:
                dict[current].append(word)
       
        current = word

    return dict

def gen(word, i, n):
    if i == n: return
    else:
        print (word, end=' '),
        next_word = random.choice(dict[word]) if word in dict else random.choice(list(dict.keys()))
        gen(next_word, i+1, n)

if __name__ == '__main__':
    with open(sys.argv[1]) as f:
        str = f.read().replace('\n', '');
        dict = construct(str)
        print(dict)
        rword = random.choice(list(dict.keys()))
        gen(rword, 0, 50)
